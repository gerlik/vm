$(document).ready(function () {

    ////----- Open the modal to CREATE new teacher -----////
    $('#btn-add-new-teacher').click(function () {
        $('#create-teacher-modal').modal('show');
    });

    $('#btn-save-new-teacher').click(function () {

        var school_id = $('#teacher-school option:selected').val();

        if ($('#teacher-name').val() === '' || $('#teacher-email').val() === ''
            || $('#teacher-role').val() === 'Vali roll...' || school_id === 'Vali kool...') {
            alert('Tühi ala ei sobi.');
        } else {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: '/people/store',
                data: {
                    name: $('#teacher-name').val(),
                    email: $('#teacher-email').val(),
                    role: $('#teacher-role').val(),
                    school_id: school_id
                },
                success: function () {
                    location.reload();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    });

    ////----- Open the modal to CREATE new student -----////
    $('#btn-add-new-student').click(function () {
        $('#create-student-modal').modal('show');
    });

    $('#btn-save-new-student').click(function () {

        var group_id = $('#student-group option:selected').val();

        if ($('#student-name').val() === '' || $('#student-email').val() === '' || group_id === 'Vali grupp...') {
            alert('Tühi ala ei sobi.');
        } else {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: '/people/store',
                data: {
                    name: $('#student-name').val(),
                    email: $('#student-email').val(),
                    role: 'student',
                    group_id: group_id
                },
                success: function () {
                    location.reload();
                },
                error: function (data) {
                    //Todo: ei õnnestunud lisada
                    console.log('Error:', data);
                }
            });
        }
    });


    ////----- Open the modal to UPDATE teacher -----////
    $('tr .open-edit-teacher-modal').on('click', function (e) {
        var teacher_id = $(this).attr('data-id');
        var teacher_name = $(this).attr('data-name');
        var email = $(this).attr('data-email');
        var role = $(this).attr('data-role');

        $('#teacher-name-new').val(teacher_name);
        $('#teacher-email-new').val(email);
        $('#teacher-role-new').val(role);

        $('#edit-teacher-modal').modal('show');

        $('#btn-save-edit-teacher').on('click', (function () {

            if ($('#teacher-name-new').val() === '') {
                alert('Tühi nimi ei sobi.');
                location.reload();
            } else {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    validate: function (value) {
                        if ($.trim(value) === '')
                            return 'Required.';
                    },
                    type: "POST",
                    url: '/people/update/' + teacher_id,
                    data: {
                        email: $('#teacher-email-new').val(),
                        name: $('#teacher-name-new').val(),
                        school_id: $('#teacher-school-new').val(),
                        role: $('#teacher-role-new').val()
                    },
                    success: function () {
                        location.reload();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        }));

    });

    ////----- Open the modal to UPDATE student -----////
    $('tr .open-edit-student-modal').on('click', function (e) {
        var student_id = $(this).attr('data-id');
        var user_id = student_id;
        var student_name = $(this).attr('data-name');
        var email = $(this).attr('data-email');

        $('#student-name-new').val(student_name);
        $('#student-email-new').val(email);

        $('#edit-student-modal').modal('show');

        $('#btn-save-edit').on('click', (function () {

            if ($('#student-name-new').val() === '') {
                alert('Tühi nimi ei sobi.');
                location.reload();
            } else {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    validate: function (value) {
                        if ($.trim(value) === '')
                            return 'Required.';
                    },
                    type: "POST",
                    url: '/people/update/' + student_id,
                    data: {
                        email: $('#student-email-new').val(),
                        name: $('#student-name-new').val(),
                        group_id: $('#student-group-new').val(),
                        role: 'student'
                    },
                    success: function () {
                        location.reload();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        }));

        $('.delete-person').on('click', (function (e) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "DELETE",
                url: '/people/delete/' + student_id,
                success: function () {
                    $("#student" + student_id).remove();
                    $('#edit-student-modal').modal('hide');
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

        }));


    });

    ////----- DELETE teacher -----////
    $('.delete-person').on('click', (function (e) {
        e.preventDefault();
        var user_id = $(this).val();

        if (confirm('Kindel et soovite selle isiku kustutada?')) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "DELETE",
                url: '/people/delete/' + user_id,
                success: function () {
                    $("#teacher" + user_id).remove();
                    $('#edit-student-modal').modal('hide');
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

    }));

    // Remove active status when switching between group tabs
    $('.nav').on('show.bs.tab', function (e) {
        $('.nav .active').removeClass('active');
    })


});