$(document).ready(function () {

    ////----- Open the modal to CREATE new school -----////
    $('#btn-add-new-school').click(function () {
        $('#create-school-modal').modal('show');
    });

    $('#btn-save-new-school').click(function () {

        if ($('#school-name').val() === '') {
            alert('Tühi nimi ei sobi.');
            location.reload();
        } else {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: '/schools/store',
                data: {
                    school_name: $('#school-name').val()
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    });

    ////----- Open the modal to UPDATE a link -----////
    $('tr .open-edit-school-modal').on('click', function (e) {
        var school_name = $(this).attr('data-name');
        $('#school-name-new').val(school_name);

        var school_id = $(this).attr('data-id');
        $('#edit-school-modal').modal('show');

        $('#btn-save').on('click', (function () {

            if ($('#school-name-new').val() === '') {
                alert('Tühi nimi ei sobi.');
                location.reload();
            } else {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    validate: function (value) {
                        if ($.trim(value) === '')
                            return 'Required.';
                    },
                    type: "POST",
                    url: '/schools/update/' + school_id,
                    data: {
                        school_id: school_id,
                        school_name: $('#school-name-new').val()
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        }));

    });

    ////----- DELETE school -----////
    $('.delete-school').on('click', (function (e) {
        e.preventDefault();
        var school_id = $(this).val();

        if (confirm('Kindel et soovite kooli kustutada?')) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "DELETE",
                url: '/schools/delete/' + school_id,
                success: function () {
                    $("#school" + school_id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

    }));

    ////----- DELETE group -----////
    $('.delete-group').on('click', (function (e) {
        var group_id = $(this).data('group_id');

        if (confirm('Kindel et soovite selle õppegrupi kustutada?')) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "DELETE",
                url: '/schools/delete/group/' + group_id,
                success: function () {
                    $("#group" + group_id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

    }));

    ////----- Open the modal to ADD a new group -----////
    $('tr .open-add-group-modal').on('click', function (e) {
        $('#add-group-modal').modal('show');

        var school_id = $(this).attr('data-id');

        $('#btn-save-group').click(function () {

            if ($('#new-group').val() === '') {
                alert('Tühi nimi ei sobi.');
                location.reload();
            } else {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: '/schools/store/group/' + school_id,
                    data: {
                        school_id: school_id,
                        group_name: $('#new-group').val()
                    },
                    success: function () {
                        location.reload();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });
    });


});
