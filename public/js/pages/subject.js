$(document).ready(function () {

    ////----- Open the modal to CREATE new subject -----////
    $('#btn-add-new-subject').click(function () {
        $('#create-subject-modal').modal('show');
    });

    $('#btn-save-new-subject').click(function () {

        if ($('#subject-name').val() === '') {
            alert('Tühi nimi ei sobi.');
            location.reload();
        } else {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: '/subjects/store',
                data: {
                    subject_name: $('#subject-name').val()
                },
                success: function () {
                    location.reload();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    });

    ////----- Open the modal to UPDATE a subject -----////
    $('tr .open-edit-subject-modal').on('click', function (e) {
        var subject_name = $(this).attr('data-name');
        $('#subject-name-new').val(subject_name);

        var subject_id = $(this).attr('data-id');
        $('#edit-subject-modal').modal('show');

        $('#btn-save-edit').on('click', (function () {

            if ($('#subject-name-new').val() === '') {
                alert('Tühi nimi ei sobi.');
                location.reload();
            } else {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    validate: function (value) {
                        if ($.trim(value) === '')
                            return 'Required.';
                    },
                    type: "POST",
                    url: '/subjects/update/' + subject_id,
                    data: {
                        subject_id: subject_id,
                        subject_name: $('#subject-name-new').val()
                    },
                    success: function () {
                        location.reload();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        }));

    });

    ////----- DELETE subject -----////
    $('.delete-subject').on('click', (function (e) {
        e.preventDefault();
        var subject_id = $(this).val();

        if (confirm('Kindel et soovite selle aine ja aine ülesanded kustutada?')) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "DELETE",
                url: '/subjects/delete/' + subject_id,
                success: function () {
                    $("#subject" + subject_id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

    }));


    ////----- DELETE task -----////
    $('.delete-task').on('click', (function (e) {
        var task_id = $(this).data('task_id');

        if (confirm('Kindel et soovite selle ülesande kustutada?')) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "DELETE",
                url: '/subjects/delete/task/' + task_id,
                success: function () {
                    $("#task" + task_id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

    }));

    ////----- Open the modal to ADD a new task -----////
    $('tr .open-add-task-modal').on('click', function (e) {
        $('#add-task-modal').modal('show');

        var subject_id = $(this).attr('data-id');

        $('#btn-save-task').click(function () {

            if ($('#new-task').val() === '') {
                alert('Tühi nimi ei sobi.');
                location.reload();
            } else {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: '/subjects/store/task/' + subject_id,
                    data: {
                        subject_id: subject_id,
                        task_name: $('#new-task').val()
                    },
                    success: function () {
                        location.reload();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });
    });


});
