$(document).ready(function () {

    ////----- CREATE new droplet -----////
    $('#create-droplet').click(function () {
        var droplet_name = $('#available-tasks option:selected').val();
        var task_id = $('#available-tasks option:selected').data('id');

        if (droplet_name === 'Vali ülesanne, mida lahendada' || droplet_name === 'Pole veel ülesandeid ...') {
            alert('Ei saa ülesandeta masinat luua.');
            location.reload();
        } else {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: '/home/droplet',
                data: {
                    task_id: task_id,
                    droplet_name: droplet_name
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    });

    ////----- DELETE selected droplets -----////
    $('#delete-droplets').click(function () {
        $('.droplets').find('tbody input:checked').each(function () {
            var droplet_id = $(this).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "DELETE",
                url: 'home/droplet/' + droplet_id,
                success: function () {
                    $("#droplet" + droplet_id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });
    });


    ////----- DELETE log -----////
    $('#delete-log').click(function () {

        if (confirm('Kindel et soovite kustutada?')) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "DELETE",
                url: 'home/delete/log',
                success: function () {
                    location.reload();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

    });

    $(".task-done-checkbox").click(function () {
        var droplet_id = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: 'home/droplet/done',
            success: function () {
                location.reload();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

    });

    $("#checkAll").change(function () {
        $('.check').attr('checked', 'checked');
    });

});
