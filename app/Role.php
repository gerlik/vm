<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public $incrementing = false;

    protected $table = 'roles';

    public $timestamps = false;

    public function users()
    {
        return $this->hasMany('App\User', 'role', 'role_name');
    }
}
