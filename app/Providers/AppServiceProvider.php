<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Blade::directive('admin', function () {
            return "<?php if(auth()->user()->hasRole('admin')): ?>";
        });

        Blade::directive('endadmin', function () {
            return "<?php endif; ?>";
        });

        // Teacher and admin
        Blade::directive('teacherandadmin', function () {
            return "<?php if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('teacher')): ?>";
        });

        Blade::directive('endteacherandadmin', function () {
            return "<?php endif; ?>";
        });
    }
}
