<?php

namespace App\Http\Controllers;

use App\Droplet;
use App\Log;
use App\Task;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();

        $tasks = Task::all();
        $droplets = Droplet::getDroplets();
        $droplet_data = Droplet::all();
        $log = Log::all();
        $users = User::all();

        if ($user['role'] === 'admin' || $user['role'] === 'teacher') {

            return view('droplets', [
                'tasks' => $tasks,
                'droplets' => $droplets,
                'droplet_data' => $droplet_data,
                'log' => $log,
                'users' => $users
            ]);

        } else {

            return view('droplets', [
                'tasks' => $tasks,
                'droplets' => $droplets,
                'droplet_data' => $droplet_data->where('owner_id', Auth::id()),
                'log' => $log,
                'users' => $users
            ]);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $drop = new Droplet();

        try {
            $droplet = $drop->createDroplet(str_slug($user['id'] . '-' . $user['name'] . '-' . $request['droplet_name']));
        } catch (Exception $message) {
            echo 'Attention: ' . $message->getMessage();
        }


        Log::create([
            'date' => date('Y-m-d H:i:s'),
            'action' => 'lõi ülesande',
            'user_id' => $user['id'],
            'task_name' => $request['droplet_name']
        ]);

        $data = [
            'id' => $droplet->id,
            'droplet_name' => $request['droplet_name'],
            'task_id' => $request['task_id'],
            'owner_id' => $user['id'],
            'status' => $droplet->status,
            'expires' => date('Y-m-d H:i:s', time() + 86400)
        ];

        $data->validate([
            'owner_id' => 'required',
            'droplet_name' => 'required|max:255',
            'task_id' => 'integer|nullable',
            'status' => 'nullable',
            'expires' => 'required',

        ]);
//        TODO: kui ei sisesatud meie süsteemi, siis saata kustutamisnõue tagasi?

        Droplet::create($data);

        return redirect()->route('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Droplet::deleteDroplet($id);
        Droplet::find($id)->delete($id);

        Log::create([
            'date' => date('Y-m-d H:i:s'),
            'action' => 'kustutas ülesande',
            'user_id' => Auth::id(),
        ]);

        return response()->json(['success' => 'Droplet deleted successfully!']);
    }

    public function deleteLog()
    {
        Log::truncate();

        return response()->json(['success' => 'Log deleted successfully!']);
    }


}
