<?php

namespace App\Http\Controllers;

use App\Droplet;
use App\Group;
use App\Mail\SendMail;
use App\School;
use App\Task;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class PeopleController extends Controller
{
    public function index()
    {
        $groups = Group::all();
        $schools = School::all();
        $students = User::where('role', 'student')->get();
        $teachers = User::where('role', '!=', 'student')->get();
        $tasks = Task::all();
        $droplets = Droplet::all();

        return view('pages.people', [
            'groups' => $groups,
            'schools' => $schools,
            'students' => $students,
            'teachers' => $teachers,
            'tasks' => $tasks,
            'droplets' => $droplets
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'group_id' => 'integer|nullable',
            'school_id' => 'integer|nullable',
            'role' => 'required',
        ]);
        $user = User::create($validatedData);
//        SchoolsTeachers::create(['school_id' => $request['school_id'], 'teacher_id' => $user['id']]);

        // Generate and send password to user's e-mail
//        $this->email($user['id']);

        return response()->json([
            'success' => 'User created successfully!'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required',
            'group_id' => 'integer|nullable',
            'school_id' => 'integer|nullable',
            'role' => 'required',
        ]);
        User::whereId($id)->update($validatedData);
//        SchoolsTeachers::where('teacher_id', $id)->update(['school_id' => $request['school_id']]);

        return response()->json(['success', 'User was successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete($id);

        return response()->json(['success' => 'User deleted successfully!']);
    }


    // Send password to e-mail, when new user is manually created
    public function email($id)
    {
        $user = User::where('id', $id)->get();
        $password = md5(rand(1, 10000));

        DB::table('users')->where('id', $id)->update(['password' => bcrypt($password)]);

        Mail::to($user[0]['email'])->send(new SendMail($user[0]['name'], $password));

        return redirect()->route('people');
    }


}
