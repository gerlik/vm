<?php

namespace App\Http\Controllers;

use App\Group;
use App\School;
use Illuminate\Http\Request;

class SchoolsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schools = School::all();
        $groups = Group::all();

        return view('pages.schools', ['schools' => $schools, 'groups' => $groups]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'school_name' => 'required|max:255',
        ]);
        $school = School::create($validatedData);

        return response()->json([
            'success' => 'School created successfully!'
        ]);
    }

    public function storeGroup(Request $request, $id)
    {
        $validatedData = $request->validate([
            'school_id' => 'required',
            'group_name' => 'required|max:255',
        ]);

        $group = Group::create($validatedData);

        return response()->json(['success', 'Group successfully created']);
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'school_name' => 'required|max:255',
        ]);
        School::whereId($id)->update($validatedData);

        return response()->json(['success', 'School successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        School::find($id)->delete($id);

        return response()->json(['success' => 'School deleted successfully!']);
    }

    public function destroyGroup($id)
    {
        Group::find($id)->delete($id);

        return response()->json(['success' => 'Group deleted successfully!']);
    }

}
