<?php

namespace App\Http\Controllers;

use App\Subject;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SubjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = Subject::all();
        $tasks = Task::all();

        return view('pages.subjects', ['subjects' => $subjects, 'tasks' => $tasks]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'subject_name' => 'required|max:255',
        ]);
        $subject = Subject::create($validatedData);

        return response()->json(['success', 'Subject was successfully created']);
    }

    public function storeTask(Request $request)
    {
        $validatedData = $request->validate([
            'subject_id' => 'required',
            'task_name' => 'required|max:255',
        ]);

        $task = Task::create($validatedData);

        return response()->json(['success', 'Task was successfully created']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'subject_name' => 'required|max:255',
        ]);
        Subject::whereId($id)->update($validatedData);

        return response()->json(['success', 'Subject was successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Subject::find($id)->delete($id);

        return response()->json(['success' => 'Subject deleted successfully!']);
    }

    public function destroyTask($id)
    {
        Task::find($id)->delete($id);

        return response()->json(['success' => 'Task deleted successfully!']);
    }
}
