<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class AuthGoogleController extends Controller
{
    /**
     * Redirect the user to the Google authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirect()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from Google.
     *
     * @return \Illuminate\Http\Response
     */
    public function callback()
    {
        try {
            $user = Socialite::driver('google')->stateless()->user();
        } catch (\Exception $e) {
            return redirect('/login');
        }

        // Only allow people with @khk.ee or vkok.ee to login
        if (explode("@", $user->email)[1] !== 'khk.ee' || explode("@", $user->email)[1] !== 'vkok.ee') {
            return redirect()->to('/');
        }

        $existingUser = User::where('email', $user->email)->first();
        if ($existingUser) {
            // log them in
            auth()->login($existingUser, true);
        } else {
            $newUser = new User;
            $newUser->name = $user->name;
            $newUser->email = $user->email;
            $newUser->google_id = $user->id;

            // TODO: Send password to email maybe
//            $user->password = md5(rand(1, 10000));
            $newUser->save();
            auth()->login($newUser, true);
        }
        return redirect()->to('/home');

    }
}
