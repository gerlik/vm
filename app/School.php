<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $table = 'schools';

    protected $fillable = ['school_id', 'school_name'];

    public $timestamps = false;

    public function schools()
    {
        return $this->hasMany('App\Group', 'school_id', 'id');
    }
}
