<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table = 'subjects';

    protected $fillable = ['subject_name'];

    public $timestamps = false;

    public function subject()
    {
        return $this->hasMany('App\Task', 'subject_id', 'id');
    }

    public function getTasks()
    {
        print_r($this->subject()->getResults());
        return $this->subject()->getResults();
    }
}
