<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';

    protected $fillable = ['school_id', 'group_name'];

    public $timestamps = false;

    public function groups()
    {
        return $this->hasMany('App\Subject', 'id', 'school_id');
    }
}
