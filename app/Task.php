<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'tasks';

    protected $fillable = ['subject_id', 'task_name', 'task_id'];

    public $timestamps = false;

    public function tasks()
    {
        return $this->hasMany('App\Subject', 'id', 'subject_id');
    }

}
