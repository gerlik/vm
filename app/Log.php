<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = 'log';

    protected $fillable = ['date', 'action', 'user_id', 'task_id'];

    public function log()
    {
        return $this->belongsTo('App\User', 'id', 'user_id');
    }

    public function status()
    {

    }
}
