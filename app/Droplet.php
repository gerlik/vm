<?php

namespace App;

use DigitalOceanV2\Adapter\GuzzleHttpAdapter;
use DigitalOceanV2\DigitalOceanV2;
use GrahamCampbell\DigitalOcean\Facades\DigitalOcean;
use Illuminate\Database\Eloquent\Model;

class Droplet extends Model
{
    protected $table = 'droplets';

    protected $fillable = ['id', 'owner_id', 'ip_address', 'task_id', 'droplet_name', 'status', 'comment_student', 'comment_teacher', 'expires'];

    protected $digitalocean;

    public static function connect()
    {
        date_default_timezone_set('Europe/Tallinn');

//        // See config/digitalocean.php
        return $droplets = DigitalOcean::region()->getAll();
    }

    public static function getDroplets()
    {
        date_default_timezone_set('Europe/Tallinn');

        $adapter = new GuzzleHttpAdapter('b568cf1aa6711761e0adc20e7425599c45d3674445cee3d6be48d116a83df264');

        // Create a digital ocean object with the previous adapter
        $digitalOcean = new DigitalOceanV2($adapter);

        // Get instance of Droplet API
        $droplet_api = $digitalOcean->droplet();
        $droplets = $droplet_api->getAll();

        return $droplets;
    }

    public function createDroplet($name)
    {
        $adapter = new GuzzleHttpAdapter('b568cf1aa6711761e0adc20e7425599c45d3674445cee3d6be48d116a83df264');

        // Create a digital ocean object
        $digitalOcean = new DigitalOceanV2($adapter);

        $droplet_api = $digitalOcean->droplet();

        $key_api = $digitalOcean->key();
        $ssh_keys = array($key_api->getById(24681146)->id);

        $droplet = $droplet_api->create($name, 'ams3', '512mb', 'ubuntu-14-04-x64', false, false, false, $ssh_keys);

        return $droplet;
    }

    public static function deleteDroplet($droplet_id)
    {
        DigitalOcean::droplet()->delete($droplet_id);
    }

    public function deleteAllDroplets($name)
    {
        $droplets = DigitalOcean::droplet()->getAll();

        foreach ($droplets as $droplet) {
            if ($droplet->name == $name) {
                DigitalOcean::droplet()->delete($droplet->id);
            }
        }
    }


}
