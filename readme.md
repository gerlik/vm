# Virtuaalmasinate haldamise keskkond / Project with virtual machines
Virtuaalmasinate haldamise keskkond
___
This is a project that uses Digital Ocean API which lets students create virtual machine droplets and teachers to check their work;
Notes for myself for now

### Projekti üles seadmine / Setup
```
composer install
copy .env.example file and name it .env in the same directory
php artisan key:generate
php artisan cache:clear
create database
update database info in .env file
php artisan migrate
php artisan db:seed
```
~~npm install~~


### Kasutus / Usage
- Kohalik server / Run server
```
php artisan serve
php artisan serve --port=8001
```

- Webpack - Laravel Mix
```bash
// Käivita Mix (loob kohalikud failid) / Run all Mix tasks
npm run dev

// Käivita kui tahad minifitseeritud faile / Run all Mix tasks and minify output
npm run production
```
Edit webpack.mix.js to add files

- Other
```
php artisan config:cache
php artisan cache:clear
php artisan route:cache
```
## Tõrked/ Troubleshooting
Viskab 419 sisse logides ja developer tools näitab, et korraks käidi /home marsruudil ära
___
Throws 419 although the credentials were correct. Says its a CSRF issue but actually sessions are broken.

```
session.php
'domain' => env('SESSION_DOMAIN', null), 
```


### Kasutajad / Users
Kasutajanimed ja paroolid testimiseks
___
Users-pws for testing different user roles in the local system
```
kasutajanimi/username: test@kasutaja.ee
kasutajanimi/username: test@opetaja.ee
kasutajanimi/username: test@opilane.ee

parool/password: kasutaja
```

## Testimine / Testing
Testimine PHPUnitiga (ühiktestid)
___
Some simple unit tests with PHPUnit

```
// Käivita testid / Run tests with
vendor/bin/phpunit
```
