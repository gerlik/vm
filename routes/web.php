<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Authentication
Route::get('/', 'Auth\LoginController@index');
Auth::routes();

// Homepage or droplets
Route::get('/home', [
    'middleware' => ['auth', 'roles'], // 'roles' middleware must be specified
    'uses' => 'HomeController@index',
    'roles' => ['admin', 'teacher', 'student'] // Only an administrator, or a teacher can access this route
])->name('home');
Route::post('/home/droplet', 'HomeController@store')->name('droplets.store');
Route::delete('/home/droplet/{droplet_id}', 'HomeController@destroy')->name('droplet.destroy');
Route::delete('/home/delete/log', 'HomeController@deleteLog')->name('log.destroy');


// People
Route::get('/people', [
    'middleware' => ['auth', 'roles'],
    'uses' => 'PeopleController@index',
    'roles' => ['admin', 'teacher']
])->name('people');
Route::post('/people/store', 'PeopleController@store')->name('people.store');
Route::post('/people/update/{user_id}', 'PeopleController@update')->name('people.update');
Route::delete('/people/delete/{user_id}', 'PeopleController@destroy')->name('people.destroy');

//Send password to e-mail
Route::get('/people/sendemail/{user_id}', 'PeopleController@email')->name('people.email');

// Schools
Route::get('/schools', [
    'middleware' => ['auth', 'roles'],
    'uses' => 'SchoolsController@index',
    'roles' => ['admin', 'teacher']
])->name('schools');
Route::post('/schools/store', 'SchoolsController@store')->name('schools.store');
Route::post('/schools/update/{school_id}', 'SchoolsController@update')->name('schools.update');
Route::delete('/schools/delete/{school_id}', 'SchoolsController@destroy')->name('schools.destroy');
Route::post('/schools/store/group/{school_id}', 'SchoolsController@storeGroup')->name('schools.storeGroup');
Route::delete('/schools/delete/group/{group_id}', 'SchoolsController@destroyGroup')->name('schools.destroyGroup');

// Subjects and tasks
Route::get('/subjects', [
    'middleware' => ['auth', 'roles'],
    'uses' => 'SubjectsController@index',
    'roles' => ['admin', 'teacher']
])->name('subjects');
Route::post('/subjects/store', 'SubjectsController@store')->name('subjects.store');
Route::post('/subjects/update/{subject_id}', 'SubjectsController@update')->name('subjects.update');
Route::delete('/subjects/delete/{subject_id}', 'SubjectsController@destroy')->name('subjects.destroy');
Route::post('/subjects/store/task/{subject_id}', 'SubjectsController@storeTask')->name('subjects.storeTask');
Route::delete('/subjects/delete/task/{task_id}', 'SubjectsController@destroyTask')->name('subjects.destroyTask');


// Google authentication
Route::get('/redirect', 'AuthGoogleController@redirect');
Route::get('/callback', 'AuthGoogleController@callback');

// Log out
Route::get('logout', 'Auth\LoginController@logout');
