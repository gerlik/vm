@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ __('Õpilased') }}</h1>
        <div class="card">
            <div class="card-header">
                <div class="nav nav-tabs card-header-tabs" id="groups-table">
                    @forelse($groups as $group)
                        <li class=" nav-item">
                            <a href="#group{{$group->id}}" data-toggle="tab" class="nav-link">{{$group->group_name}}</a>
                        </li>
                    @empty
                        Gruppe pole
                    @endforelse
                </div>
            </div>
            <div class="card-body tab-content">
                @forelse($groups as $group)
                    <div class="tab-pane" id="group{{$group->id}}">
                        <table class="table table-striped table-responsive w-100 d-block d-md-table">
                            <thead>
                            <tr>
                                {{--<th><input type="checkbox" name="" value=""></th>--}}
                                <th>{{ __('ID') }}</th>
                                <th>{{ __('Nimi') }}</th>
                                @forelse($tasks as $task)
                                    <th>{{ $task->task_name }}</th>
                                @empty
                                    <th>Pole ülesandeid veel</th>
                                @endforelse
                            </tr>
                            </thead>
                            <tbody>
                            <tr>

                            @forelse($students as $student)
                                @if($student->group_id === $group->id)
                                    <tr id="student{{$student->id}}">
                                        {{--<td><input type="checkbox" name="" value=""></td>--}}
                                        <td>{{ $student->id }}</td>
                                        <td>{{ $student->name }}
                                            <button class="btn btn-light open-edit-student-modal"
                                                    data-id="{{ $student->id }}" data-name="{{ $student->name }}"
                                                    data-email="{{ $student->email }}"
                                                    data-group="{{ $student->group_id }}">
                                                <i class="fa fa-pencil-alt"></i>
                                            </button>
                                        </td>
                                        <td>
                                            @forelse($droplets as $droplet)
                                                @if($student->id === $droplet->owner_id)
                                                    @if($droplet->status === 'done')
                                                        <span class="alert-success">{{$droplet->status}}</span>
                                                    @else
                                                        <span class="alert-danger">{{$droplet->status}}</span>
                                                    @endif
                                                @endif
                                            @empty
                                                {{'-'}}
                                            @endforelse
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                @endif
                            @empty
                                <tr>
                                    <td colspan="3">{{ __('Pole õpilasi veel') }}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                @empty
                    Gruppe pole
                @endforelse
            </div>

        </div>
        <br>
        <a id="btn-add-new-student" class="btn btn-outline-primary add-student float-right">
            {{ __('+ Lisa õpilane') }}
        </a>


        {{-- TEACHERS --}}
        {{-- Only the admin can edit teachers table--}}
        <br>
        <br>
        @admin
        <h1>{{ __('Õpetajad') }}</h1>
        <div>
            <table class="table table-striped table-responsive w-100 d-block d-md-table">
                <thead>
                <tr>
                    <th>{{ __('ID') }}</th>
                    <th>{{ __('Nimi') }}</th>
                    <th>{{ __('Roll') }}</th>
                    <th>{{ __('Kool') }}</th>
                    <th>{{ __('E-mail') }}</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @forelse ($teachers as $teacher)
                    <tr id="teacher{{$teacher->id}}">
                        <td>{{ $teacher->id }}</td>
                        <td>{{ $teacher->name }}</td>
                        <td>{{ $teacher->role }}</td>
                        <td>
                            @forelse($schools as $school)
                                @if($school->id===$teacher->school_id)
                                    {{$school->school_name}}
                                @endif
                            @empty
                                {{ __('-') }}
                            @endforelse
                        </td>
                        <td>{{ $teacher->email }}</td>
                        <td>
                            <button class="btn btn-light open-edit-teacher-modal"
                                    data-id="{{ $teacher->id }}" data-email="{{ $teacher->email }}"
                                    data-name="{{ $teacher->name }}"
                                    data-role="{{ $teacher->role }}">
                                <i class="fa fa-pencil-alt"></i> {{__('Muuda')}}
                            </button>
                        </td>
                        <td>
                            <button class="btn btn-outline-danger delete-person"
                                    value="{{ $teacher->id }}">
                                {{ __('Kustuta') }}
                            </button>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6">{{ __('Pole õpetajaid veel') }}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>

            <a id="btn-add-new-teacher" class="btn btn-outline-primary add-teacher float-right">
                {{ __('+ Lisa õpetaja') }}
            </a>
        </div>
        @endadmin

    </div>


    <!-- Create teacher Modal -->
    <div class="modal fade" id="create-teacher-modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title"><b>{{ __('Loo uus õpetaja') }}</b></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            {{--<div class="alert alert-info" role="alert">--}}
                            {{--{{ __('Parool saadetakse kasutajale sisestatud e-mailile') }}--}}
                            {{--</div>--}}

                            <label for="teacher-name">{{ __('Nimi') }}</label>
                            <input type="text" class="form-control" name="teacher-name" id="teacher-name">

                            <label for="teacher-email">{{ __('E-mail') }}</label>
                            <input type="text" class="form-control" name="teacher-email" id="teacher-email">

                            <label for="teacher-school">{{ __('Kool') }}</label>
                            <select type="text" class="form-control" name="teacher-school" id="teacher-school">
                                <option selected>{{ __('Vali kool...') }}</option>
                                @forelse($schools as $school)
                                    <option value="{{$school->id}}">{{$school->school_name}}</option>
                                @empty
                                    {{ __('Koole pole') }}
                                @endforelse
                            </select>

                            <label for="teacher-role">{{ __('Roll') }}</label>
                            <select type="text" class="form-control" name="teacher-role" id="teacher-role">
                                <option selected>{{ __('Vali roll...') }}</option>
                                <option>teacher</option>
                                <option>admin</option>
                            </select>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">{{ __('Sulge') }}</button>
                        <button type="submit" id="btn-save-new-teacher"
                                class="btn btn-primary">{{ __('Salvesta') }}</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- /Create teacher Modal -->

    <!-- Create student Modal -->
    <div class="modal fade" id="create-student-modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title"><b>{{ __('Loo uus õpilane') }}</b></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            {{--<div class="alert alert-info" role="alert">--}}
                            {{--{{ __('Parool saadetakse kasutajale sisestatud e-mailile') }}--}}
                            {{--</div>--}}

                            <label for="student-name">{{ __('Nimi') }}</label>
                            <input type="text" class="form-control" name="student-name" id="student-name">

                            <label for="student-email">{{ __('E-mail') }}</label>
                            <input type="text" class="form-control" name="student-email" id="student-email">

                            <label for="student-group">{{ __('Grupp') }}</label>
                            <select type="text" class="form-control" name="student-group" id="student-group">
                                <option selected>{{ __('Vali grupp...') }}</option>
                                @forelse($groups as $group)
                                    <option value="{{$group->id}}">{{$group->group_name}}</option>
                                @empty
                                    {{ __('Gruppe pole') }}
                                @endforelse
                            </select>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">{{ __('Sulge') }}</button>
                        <button type="submit" id="btn-save-new-student"
                                class="btn btn-primary">{{ __('Salvesta') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Create student Modal -->

    <!-- Edit student Modal -->
    <div id="edit-student-modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title"><b>{{ __('Muuda õpilase infot') }}</b></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="student-name-new">{{ __('Nimi') }}</label>
                            <input type="text" class="form-control" name="student-name-new" id="student-name-new">

                            <label for="student-email-new">{{ __('E-mail') }}</label>
                            <input type="text" class="form-control" name="student-email-new" id="student-email-new">

                            <label for="student-group-new">{{ __('Grupp') }}</label>
                            <select type="text" class="form-control" name="student-group-new" id="student-group-new">
                                @forelse($groups as $group)
                                    <option value="{{$group->id}}">{{$group->group_name}}</option>
                                @empty
                                    {{ __('Koole pole') }}
                                @endforelse
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger delete-person mr-auto">
                            {{ __('Kustuta') }}
                        </button>
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">{{ __('Sulge') }}</button>
                        <button type="submit" id="btn-save-edit"
                                class="btn btn-primary">{{ __('Salvesta') }}</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- /Edit student Modal -->

    <!-- Edit teacher Modal -->
    <div id="edit-teacher-modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title"><b>{{ __('Muuda õpetaja infot') }}</b></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="teacher-name-new">{{ __('Nimi') }}</label>
                            <input type="text" class="form-control" name="teacher-name-new" id="teacher-name-new">

                            <label for="teacher-email-new">{{ __('E-mail') }}</label>
                            <input type="text" class="form-control" name="teacher-email-new" id="teacher-email-new">

                            <label for="teacher-school-new">{{ __('Kool') }}</label>
                            <select type="text" class="form-control" name="teacher-school-new" id="teacher-school-new">
                                @forelse($schools as $school)
                                    <option value="{{$school->id}}">{{$school->school_name}}</option>
                                @empty
                                    {{ __('Koole pole') }}
                                @endforelse
                            </select>

                            <label for="teacher-role-new">{{ __('Roll') }}</label>
                            <select type="text" class="form-control" name="teacher-role-new" id="teacher-role-new">
                                <option>teacher</option>
                                <option>admin</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">{{ __('Sulge') }}</button>
                        <button type="submit" id="btn-save-edit-teacher"
                                class="btn btn-primary">{{ __('Salvesta') }}</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- /Edit teacher Modal -->

@endsection

@section('footer_script')
    <script type="text/javascript" src="{{asset('js/common/jquery-3.4.1.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/pages/people.js')}}"></script>
@endsection