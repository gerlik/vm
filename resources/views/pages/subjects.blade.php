@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ __('Ained') }}</h1>
        <div>
            <table class="table table-striped table-responsive w-100 d-block d-md-table">
                <thead>
                <tr>
                    <th>{{ __('ID') }}</th>
                    <th>{{ __('Aine nimi') }}</th>
                    <th>{{ __('Ülesanded') }}</th>
                    <th>{{ __('') }}</th>
                    <th>{{ __('') }}</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($subjects as $key => $subject)
                    <tr id="subject{{$subject->id}}">
                        <td>{{ $subject->id }}</td>
                        <td>{{ $subject->subject_name }}</td>
                        <td>
                            @forelse($tasks as $index => $task)
                                @if($task->subject_id == $subject->id)
                                    <span class="badge badge-success" id="task{{$task->id}}">{{ $task->task_name }}
                                        <a class="fa fa-times-circle delete-task" data-task_id="{{$task->id}}"></a>
                                    </span>
                                @endif
                            @empty
                                {{--<span>{{__('Ülesandeid pole veel')}}</span>--}}
                            @endforelse
                            <button class="btn btn-outline-success open-add-task-modal"
                                    data-id="{{ $subject->id }}">
                                {{__('+ Lisa ülesanne')}}
                            </button>
                        </td>
                        <td>
                            <button class="btn btn-light open-edit-subject-modal"
                                    data-id="{{ $subject->id }}" data-name="{{ $subject->subject_name }}">
                                <i class="fa fa-pencil-alt"></i> {{__('Muuda')}}
                            </button>
                        </td>
                        <td>
                            <button class="btn btn-outline-danger delete-subject"
                                    value="{{ $subject->id }}">
                                {{ __('Kustuta') }}
                            </button>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5">{{ __('Pole aineid veel') }}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>

            <a id="btn-add-new-subject" class="btn btn-outline-primary add-friend float-right">
                {{ __('+ Lisa aine') }}
            </a>
        </div>
    </div>

    <!-- Create subject Modal -->
    <div class="modal fade" id="create-subject-modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title"><b>{{ __('Loo uus aine') }}</b></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="subject-name">{{ __('Aine nimi') }}</label>
                            <input type="text" class="form-control" name="subject-name" id="subject-name">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">{{ __('Sulge') }}</button>
                        <button type="submit" id="btn-save-new-subject"
                                class="btn btn-primary">{{ __('Salvesta') }}</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- /Create subject Modal -->

    <!-- Edit subject Modal -->
    <div id="edit-subject-modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title"><b>{{ __('Muuda ainet') }}</b></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="subject">{{ __('Aine nimi') }}</label>
                            <input type="text" class="form-control" name="subject-name-new" id="subject-name-new">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">{{ __('Sulge') }}</button>
                        <button type="submit" id="btn-save-edit"
                                class="btn btn-primary">{{ __('Salvesta') }}</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- /Edit subject Modal -->

    <!-- Add task Modal -->
    <div id="add-task-modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title"><b>{{ __('Lisa uus ülesanne') }}</b></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="new-task">{{ __('Uue ülesande nimi') }}</label>
                            <input type="text" class="form-control" name="new-task" id="new-task">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">{{ __('Sulge') }}</button>
                        <button type="submit" id="btn-save-task"
                                class="btn btn-primary">{{ __('Salvesta') }}</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- /Add task modal Modal -->

@endsection

@section('footer_script')
    <script type="text/javascript" src="{{asset('js/common/jquery-3.4.1.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/pages/subject.js')}}"></script>
@endsection
