@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ __('Koolid') }}</h1>
        <div>
            <table class="table table-striped table-responsive w-100 d-block d-md-table">
                <thead>
                <tr>
                    <th>{{ __('ID') }}</th>
                    <th>{{ __('Nimi') }}</th>
                    <th>{{ __('Grupid') }}</th>
                    <th>{{ __('') }}</th>
                    <th>{{ __('') }}</th>
                </tr>
                </thead>
                <tbody>
                @forelse($schools as $key => $school)
                    <tr id="school{{$school->id}}">
                        <td>{{ $school->id }}</td>
                        <td>{{ $school->school_name }}</td>
                        <td>
                            @forelse($groups as $index => $group)
                                @if($group->school_id == $school->id)
                                    <span class="badge badge-success" id="group{{$group->id}}">
                                        {{ $group->group_name }}
                                        @admin
                                        <a class="fa fa-times-circle delete-group" data-group_id="{{$group->id}}"></a>
                                        @endadmin
                                    </span>
                                @endif
                            @empty
                                {{--<span>{{__('Õppegruppe pole veel')}}</span>--}}
                            @endforelse
                            @admin
                            <button class="btn btn-outline-success open-add-group-modal"
                                    data-id="{{ $school->id }}">
                                {{__('+ Lisa grupp')}}
                            </button>
                            @endadmin
                        </td>
                        <td>
                            @admin
                            <button class="btn btn-light open-edit-school-modal"
                                    data-id="{{$school->id}}" data-name="{{ $school->school_name }}">
                                <i class="fa fa-pencil-alt"></i> {{__('Muuda')}}
                            </button>
                            @endadmin
                        </td>
                        <td>
                            @admin
                            <button class="btn btn-outline-danger delete-school"
                                    value="{{ $school->id }}">
                                {{ __('Kustuta') }}
                            </button>
                            @endadmin
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5">{{ __('Pole koole veel') }}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            @admin
            <a id="btn-add-new-school" class="btn btn-outline-primary add-friend float-right">
                {{ __('+ Lisa kool') }}
            </a>
            @endadmin
        </div>
    </div>

    <!-- Create school Modal -->
    <div class="modal fade" id="create-school-modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title"><b>{{ __('Loo uus kool') }}</b></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form id="modalFormSchoolData" name="modalFormSchoolData">
                        @csrf
                        <div class="box-body">
                            <div class="form-group">
                                <label for="school_name">{{ __('Kooli nimi') }}</label>
                                <input type="text" class="form-control" name="school_name" id="school-name">
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default"
                                    data-dismiss="modal">{{ __('Sulge') }}</button>
                            <button type="submit" id="btn-save-new-school"
                                    class="btn btn-primary">{{ __('Salvesta') }}</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <!-- /Create school Modal -->

    <!-- Edit school Modal -->
    <div id="edit-school-modal" class="modal fade edit-school-modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title"><b>{{ __('Muuda kooli') }}</b></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form>
                        @csrf
                        <div class="box-body">
                            <div class="form-group">
                                <label for="school-name-new">{{ __('Kooli nimi') }}</label>
                                <input type="text" class="form-control" name="school-name-new" id="school-name-new">
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default"
                                    data-dismiss="modal">{{ __('Sulge') }}</button>
                            <button type="submit" id="btn-save"
                                    class="btn btn-primary">{{ __('Salvesta') }}</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <!-- /Edit school Modal -->

    <!-- Add group Modal -->
    <div id="add-group-modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title"><b>{{ __('Lisa uus õppegrupp') }}</b></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="new-group">{{ __('Uue õppegrupi nimi') }}</label>
                            <input type="text" class="form-control" name="new-group" id="new-group">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">{{ __('Sulge') }}</button>
                        <button type="submit" id="btn-save-group"
                                class="btn btn-primary">{{ __('Salvesta') }}</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- /Add group modal Modal -->

@endsection

@section('footer_script')
    <script type="text/javascript" src="{{asset('js/common/jquery-3.4.1.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/pages/school.js')}}"></script>
@endsection