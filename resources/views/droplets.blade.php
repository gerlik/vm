@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>{{ __('Ülesanne') }}</h2>
                <br>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="available-tasks" style="display: none"></label>
                        <select type="text" class="form-control " name="available-tasks" id="available-tasks">
                            <option selected>{{ __('Vali ülesanne, mida lahendada') }}</option>
                            @forelse($tasks as $task)
                                <option value="{{$task->task_name}}"
                                        data-id="{{$task->id}}">{{$task->task_name}}</option>
                            @empty
                                <option>{{__('Pole veel ülesandeid ...')}}</option>
                            @endforelse
                        </select>
                        <br>
                    </div>

                    <div class="col-sm-6">
                        <a id="create-droplet" name="create" class="btn btn-primary col-md-4">
                            {{ __('Loo droplet') }}
                        </a>
                    </div>
                </div>

                <br>
                <br>
                <div class="alert alert-danger" role="alert">
                    {{ __('Kui masinat 24 tunni jooksul üles ei keerata, siis see kustub ära!') }}
                </div>
                <h2>{{ __('Dropletid') }}</h2>
                <div>
                    <table class="table table-striped table-responsive w-100 d-block d-md-table droplets">
                        <thead>
                        <tr>
                            <th><input type="checkbox" id="checkAll"></th>
                            <th>#</th>
                            <th>{{ __('Nimi') }}</th>
                            <th>{{ __('Aeg') }}</th>
                            <th>{{ __('IP aadress') }}</th>
                            <th>{{ __('Viimane tegevus') }}</th>
                            <th>{{ __('Staatus') }}</th>
                            <th>{{ __('Kommentaarid') }}</th>
                            <th>{{ __('Aegub') }}</th>
                            <th>{{ __('Valmis') }}</th>
                            <th>{{ __('') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($droplets as $droplet)
                            <tr id="droplet_{{$droplet->id}}">
                                <td><input class="check" type="checkbox" value="{{$droplet->id}}"></td>
                                <td>{{ $droplet->id }}</td>
                                <td>{{ $droplet->name }}</td>
                                <td class="timer" data-date=""></td>
                                <td>{{ $droplet->networks[0]->ipAddress }}</td>
                                <td class="action">Tegevus</td>
                                <td>{{ $droplet->status }}</td>
                                <td class="comments">
                                    <form action="">
                                        @csrf
                                        <input type="text">
                                    </form>
                                </td>
                                <td>{{ $droplet->createdAt}}</td>
                                <td class="task-done"><input type="checkbox" class="task-done-checkbox"
                                                             value="{{$droplet->id}}"></td>
                                <td>
                                    <button type="button" class="btn btn-success">{{__('Keri')}}</button>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="11">{{__('Töötavaid masinaid pole veel')}}</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
                <div>
                    <button type="button" class="btn btn-outline-danger col-md-2" id="delete-droplets">
                        {{__('Kustuta')}}
                    </button>
                    <button type="button" class="btn btn-success col-md-2">
                        {{__('Lähtesta')}}
                    </button>
                    <button type="button" class="btn btn-primary col-md-2">
                        {{__('Taaskäivita')}}
                    </button>
                </div>


                <br>
                <br>
                <br>
                @teacherandadmin
                <div>
                    <button type="button" class="btn btn-outline-danger col-md-2 float-right" id="delete-log">
                        {{__('Tühjenda')}}
                    </button>
                    <h2>{{ __('Logi') }}</h2>
                </div>
                <div>
                    <table class="table table-striped table-responsive w-100 d-block d-md-table">
                        <thead>
                        <tr>
                            <th>{{ __('Aeg') }}</th>
                            <th>{{ __('Sündmus') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($log->reverse() as $logitem)
                            @forelse($users as $user)
                                @if($logitem->user_id === $user->id)
                                    <tr>
                                        <td>{{ $logitem->date }}</td>
                                        <td>{{ $user->name.' sooritas: '. $logitem->action .' ' }}</td>
                                    </tr>
                                @endif
                            @empty
                                {{__('-')}}
                            @endforelse
                        @empty
                            <tr>
                                <td colspan="2">{{__('Logi on tühi')}}</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
                @endteacherandadmin

            </div>
        </div>
    </div>
@endsection

@section('footer_script')
    <script type="text/javascript" src="{{asset('js/common/jquery-3.4.1.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/pages/droplet.js')}}"></script>
@endsection
