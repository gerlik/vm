<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Parool peab olema vähemalt 8 tähemärki pikk.',
    'reset' => 'Muudetud!',
    'sent' => 'Link saadeti e-posti aadressile!',
    'token' => 'Vale token.',
//    'user' => 'Sellise e-mailiga kasutajat ei eksisteeri.',

];
