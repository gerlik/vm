<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools_teachers', function (Blueprint $table) {
            $table->integer('school_id')->unsigned();
            $table->foreign('school_id')
                ->references('id')
                ->on('schools');
            $table->integer('teacher_id')->unsigned();
            $table->foreign('teacher_id')
                ->references('id')
                ->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools_teachers');
    }
}
