<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SchoolsTeachersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //        if (App::environment('local')) {

        DB::table('schools_teachers')->insert([
            'teacher_id' => 1,
            'school_id' => 1
        ]);

        DB::table('schools_teachers')->insert([
            'teacher_id' => 2,
            'school_id' => 2
        ]);

        //        }
    }
}
