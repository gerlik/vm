<?php

use App\School;
use Illuminate\Database\Seeder;

class SchoolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //        if (App::environment() === 'local') {

//            DB::table('subjects')->truncate();

        School::create([
            'school_name' => 'Tartu KHK',
        ]);

        School::create([
            'school_name' => 'Kaseotsa kool',
        ]);

        School::create([
            'school_name' => 'Kuuseotsa kool',
        ]);

//        }
    }
}
