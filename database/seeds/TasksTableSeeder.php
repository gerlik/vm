<?php

use App\Task;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App::environment() === 'local') {

//            DB::table('tasks')->truncate();

            Task::create([
                'subject_id' => 1,
                'task_name' => 'Wordpressi ülesanne',
            ]);

            Task::create([
                'subject_id' => 1,
                'task_name' => 'Testimine',
            ]);

            Task::create([
                'subject_id' => 1,
                'task_name' => 'Ülesanne',
            ]);

        }
    }
}
