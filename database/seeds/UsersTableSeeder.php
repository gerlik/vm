<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

/**
 * Created by PhpStorm.
 * User: Gerli
 * Date: 23.04.2019
 * Time: 21:50
 */
class UsersTableSeeder extends Seeder
{
    public function run()
    {
//        if (App::environment('local')) {

        User::create([
            'name' => 'Test Kasutaja',
            'email' => 'test@kasutaja.ee',
            'role' => 'admin',
            'school_id' => 1,
            'google_id' => null,
            'password' => bcrypt('kasutaja'),
            'remember_token' => str_random(10)
        ]);

        User::create([
            'name' => 'Test Õpetaja',
            'email' => 'test@opetaja.ee',
            'role' => 'teacher',
            'school_id' => 1,
            'google_id' => null,
            'password' => bcrypt('kasutaja'),
            'remember_token' => str_random(10)
        ]);

        User::create([
            'name' => 'Test Õpilane',
            'email' => 'test@opilane.ee',
            'role' => 'student',
            'google_id' => null,
            'group_id' => 1,
            'password' => bcrypt('kasutaja'),
            'remember_token' => str_random(10)
        ]);

        User::create([
            'name' => 'Test Õpilane 2',
            'email' => 'test@opilne.ee',
            'role' => 'student',
            'google_id' => null,
            'group_id' => 1,
            'password' => bcrypt('kasutaja'),
            'remember_token' => str_random(10)
        ]);

        User::create([
            'name' => 'Test Õpilane 3 ',
            'email' => 'test@opine.ee',
            'role' => 'student',
            'google_id' => null,
            'group_id' => 2,
            'password' => bcrypt('kasutaja'),
            'remember_token' => str_random(10)
        ]);

        User::create([
            'name' => 'Test Õpilane 4',
            'email' => 'test@opilane.e',
            'role' => 'student',
            'google_id' => null,
            'group_id' => 4,
            'password' => bcrypt('kasutaja'),
            'remember_token' => str_random(10)
        ]);

//        }

    }

}