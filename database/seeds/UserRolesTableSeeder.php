<?php

use App\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

//        if (App::environment() === 'production') {
//            exit('You are in production mode not local');
//        }

//        DB::table('roles')->truncate();

        Role::create([
            'role_name' => 'admin',
            'id' => 1
        ]);
        Role::create([
            'role_name' => 'teacher',
            'id' => 2
        ]);
        Role::create([
            'role_name' => 'student',
            'id' => 3
        ]);


    }
}
