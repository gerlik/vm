<?php

use App\Subject;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        if (App::environment() === 'local') {

//            DB::table('subjects')->truncate();

        Subject::create([
            'subject_name' => 'wordpress',
        ]);

        Subject::create([
            'subject_name' => 'nginx',
        ]);

//        }
    }
}
