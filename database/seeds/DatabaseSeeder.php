<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserRolesTableSeeder::class);
        $this->call(SchoolsTableSeeder::class);
        $this->call(SubjectsTableSeeder::class);
        $this->call(TasksTableSeeder::class);
        $this->call(GroupsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(DropletsTableSeeder::class);
//        $this->call(SchoolsTeachersSeeder::class);

    }
}
