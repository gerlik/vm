<?php

use App\Group;
use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Group::create([
            'school_id' => 1,
            'group_name' => 'VS17',
        ]);

        Group::create([
            'school_id' => 1,
            'group_name' => 'VS18',
        ]);

        Group::create([
            'school_id' => 2,
            'group_name' => 'Esimene õppegrupp',
        ]);

        Group::create([
            'school_id' => 3,
            'group_name' => '12B klass',
        ]);
    }
}
